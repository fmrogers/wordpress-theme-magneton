<?php

/* Translation File
****************************************************************************************************************/

load_child_theme_textdomain( 'magneton', get_stylesheet_directory() . '/inc/languages' );

/* Includes
****************************************************************************************************************/

require_once( get_template_directory() . '/framework/inc/breadcrumbs.php' ); // load Breadcrumbs
require_once( get_template_directory() . '/framework/inc/banner-image.php' ); // get featured image url
require_once( get_template_directory() . '/framework/inc/title.php' ); // get the page title


/* THEME SETUP
****************************************************************************************************************
- Adds theme support
- Registers navigation menus
****************************************************************************************************************/

function magneton_theme_setup() {

    add_theme_support( 'post-formats', array( 'gallery', 'link', 'image', 'video', 'quote' ) ); // post formats
    add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );// post thumbnails
    add_theme_support( 'html5' );// HTML5 
    add_theme_support( 'automatic-feed-links' );// automatic feed links 
    
    register_nav_menus( array(
        'topbar'    => __( 'Topbar', 'magneton' ),
        'main'      => __( 'Main', 'magneton' ),        
        'mobile'    => __( 'Mobile', 'magneton' ),
        'sidebar'   => __( 'Sidebar', 'magneton' ),
        'footer'    => __( 'Footer', 'magneton' )
    ) );      

 }
 add_action( 'after_setup_theme', 'magneton_theme_setup');


/* ENQUEUE STYLESHEETS
****************************************************************************************************************
- Enqueue Boostrap
- Enqueue Font Awesome
- Enqueue Magneton style 
****************************************************************************************************************/

function magneton_stylsheets() {

    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.7', 'all' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css', false, '4.7.0', 'all' );     
    wp_enqueue_style( 'magneton', get_template_directory_uri() . '/style.css', array('bootstrap', 'font-awesome'), '1.0.0', 'all');

}
add_action( 'wp_enqueue_scripts', 'magneton_stylsheets' );
 

/* ENQUEUE SCRIPTS
****************************************************************************************************************
- HoverIntent
- Superfish
- Magnav
- Magneton scripts
- Comment reply script
****************************************************************************************************************/
function magneton_scripts() {  

    wp_enqueue_script( 'hoverintent', get_template_directory_uri() . '/js/hoverIntent.js', array( 'jquery' ), '1.9.0', true );
    wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.min.js', array( 'jquery', 'hoverintent' ), '1.7.9', true );
    wp_enqueue_script( 'magnav', get_template_directory_uri() . '/js/magnav.js', array( 'jquery' ), '1.0.0', true );    
    wp_enqueue_script( 'magneton', get_template_directory_uri() . '/js/magneton.js', array( 'jquery', 'hoverintent', 'superfish', 'magnav' ), '1.0.0', true );

    // Conditionals scripts
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'magneton_scripts' );


/* REGISTER SIDEBARS
****************************************************************************************************************
- Blog Page Sidebar
- Footer Column One
- Footer Column Two
- Footer Column Three
****************************************************************************************************************/
function magneton_wigets_init() {

    // Blog Sidebar
    register_sidebar( array(
        'name'          => __( 'Blog Post Sidebar', 'magneton' ),
        'id'            => 'blog-post-sidebar',
        'description'   => "Sidebar for blog specific widgets",
        'class'         => 'blog-sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>'
    ) );
    // Page Sidebar
    register_sidebar( array(
        'name'          => __( 'Page Sidebar', 'magneton' ),
        'id'            => 'page-sidebar',
        'description'   => "Sidebar for page specific widgets",
        'class'         => 'page-sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>'
    ) );
    // Footer Sidebar (column 1) 
    register_sidebar( array( 
        'name'          => __( 'Footer Column One', 'magneton' ),
        'id'            => 'footer-sidebar-one',
        'description'   => "Sidebar for blog specific widgets",
        'class'         => 'footer-col-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>'    
    ) );
    // Footer Sidebar (column 2) 
    register_sidebar( array( 
        'name'          => __( 'Footer Column Two', 'magneton' ),
        'id'            => 'footer-sidebar-two',
        'description'   => "Sidebar for blog specific widgets",
        'class'         => 'footer-col-2',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>'    
    ) );
    // Footer Sidebar (column 3) 
    register_sidebar( array( 
        'name'          => __( 'Footer Column Three', 'magneton' ),
        'id'            => 'footer-sidebar-three',
        'description'   => "Sidebar for blog specific widgets",
        'class'         => 'footer-col-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>'    
    ) );
    // Footer Sidebar (column 4) 
    register_sidebar( array( 
        'name'          => __( 'Footer Column Four', 'magneton' ),
        'id'            => 'footer-sidebar-four',
        'description'   => "Sidebar for blog specific widgets",
        'class'         => 'footer-col-4',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>'    
    ) ); 

} 
add_action( 'widgets_init', 'magneton_wigets_init' );


/* ADDITIONAL IMAGE SIZES
****************************************************************************************************************
- Blog large
- Blog medium 
- Blog small
****************************************************************************************************************/

function magneton_image_sizes() {

    add_image_size( 'blog-large', 1170, 550, array( 'center', 'top' ) );
    add_image_size( 'blog-medium', 720, 380, array( 'center', 'top' ) ); 
    add_image_size( 'blog-small', 120, 100, array( 'center', 'top' ) );   

}
add_action( 'after_setup_theme', 'magneton_image_sizes' );


/* FILTER EXCERPT
****************************************************************************************************************
- Excerpt Length
****************************************************************************************************************/

function magneton_excerpt_length() {

    if ( is_search() || is_archive() ) {
        return 35;
    } else {
        return 75;
    }
} 
add_filter( 'excerpt_length', 'magneton_excerpt_length' );


/* FILTER EXCERPT
****************************************************************************************************************
 - Excerpt 'read more' link
****************************************************************************************************************/

function magneton_excerpt_more() {
    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>', get_permalink( get_the_ID() ), __( '...read more', 'magneton' ) ); 
} 
add_filter( 'excerpt_more', 'magneton_excerpt_more' );


/* CUSTOM SEARCH WIDGET
****************************************************************************************************************
- Replaces default markup for WordPress search widget.
****************************************************************************************************************/

function magneton_search_widget( $form ) {    

    $form  = '<form role="search" method="get" class="search-form" action="' . home_url( '/' ) . '">';
    $form .=    '<label class="sr-only">Search for</label>';
    $form .=    '<input type="search" class="form-control search-field" placeholder="Search..." value name="s">';
    $form .=    '<i class="fa fa-search search-icon" aria-hidden="true"></i>';    
    $form .= '</form>';

    return $form;

}
add_filter( 'get_search_form', 'magneton_search_widget' );


/* CUSTOM COMMENT FORM
****************************************************************************************************************
- Replaces default markup for the WordPress comment form.
****************************************************************************************************************/

function magneton_comment_form_fields( $fields ) {    
    $req            = get_option( 'require_name_email' );
    $aria_req       = ( $req ? ' aria-required="true"' : '' );

    $fields = array(
        
        'author'        => '<div class="form-group form-inline form-meta-fields">
                            <label for="comment-author" class="sr-only">' . __( 'Author', 'magneton' ) . '</label>
                            <input type="text" name="author" id="comment-author" class="form-control author-field meta-field" placeholder="' . __( 'Author *', 'magneton' ) . '"' . $aria_req . '>',
        'email'         => '<label for="comment-author-email" class="sr-only">' . __( 'E-Mail', 'magneton' ) . '</label>
                            <input type="email" name="email" id="comment-author-email" class="form-control email-field meta-field" placeholder="' . __( 'E-Mail *', 'magneton' ) . '"' . $aria_req . '>',
        'url'           => '<label for="comment-author-url" class="sr-only">' . __( 'Website', 'magneton' ) . '</label>
                            <input type="url" name="url" id="comment-author-url" class="form-control url-field meta-field" placeholder="' . __( 'Website', 'magneton' ) . '">
                            </div>',
        'comment_field' => '<textarea name="comment" id="comment" class="form-control comment-field"  aria-required="true" rows="10"></textarea>'
    );

    return $fields;    

}
add_filter( 'comment_form_default_fields', 'magneton_comment_form_fields' );


/* REPLACE DEFAULT COMMENT FORM
****************************************************************************************************************
- Replaces default markup for the WordPress comment form.
****************************************************************************************************************/

function magneton_comment_form_defaults( $defaults ) {

    if ( isset( $defaults[ 'comment_field' ] ) ) {
        $defaults[ 'comment_field' ] = '';
    }

    return $defaults;

}
add_filter( 'comment_form_defaults', 'magneton_comment_form_defaults' ); 


/* REPLACE DEFAULT COMMENT LIST (Walker Class)
****************************************************************************************************************
- Replaces default markup for the WordPress comment list.
****************************************************************************************************************/

function magneton_comment_list( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment; ?>

    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix"> 
                
            <div class="avatar">
                <?php echo get_avatar($comment, $size = '75'); ?>
            </div>
                     
            <div class="comment-text">         
                <div class="author">
                    <?php if($comment->comment_author_url == '' || $comment->comment_author_url == 'http://Website'){ echo get_comment_author(); } else { echo comment_author_link(); } ?>
                </div>            
                <div class="date">
                    <em><?php printf(__('%1$s at %2$s', 'magneton'), get_comment_date(),  get_comment_time() ) ?></a><?php edit_comment_link( __( '(Edit)', 'magneton'),'  ','' ) ?>
                    &middot; <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></em>
                </div>			 
                <div class="text">
                    <?php comment_text() ?>
                </div>

                <?php if ( $comment->comment_approved == '0' ) : ?>
                    <em><?php _e( 'Your comment is awaiting moderation.', 'magneton' ) ?></em>
                    <br />
                <?php endif; ?>                  	
            </div><!-- Ends .comment-text -->    
        </div><!-- Ends #comment-{ID} -->
    </li><!-- Ends #li-comment-{ID} --> 
<?php
}