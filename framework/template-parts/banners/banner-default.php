<?php
/**
 * Displays default banner with conditional image background
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<?php if ( is_front_page() && !is_home() ) : ?>

    <section id="site-banner" class="default-banner homepage-image" role="banner" style="background-image: url('<?php echo magneton_banner( 'large' ) ?>');">
        <div class="container">
            <div class="row">        
                <div class="col-xs-12">
                    <h2 class="site-description"><?php echo get_bloginfo( 'description', 'display' ); ?></h2>
                </div><!-- .col-md-12 -->
            </div><!-- Ends .row -->        
        </div><!-- Ends .container -->
    </section><!-- Ends #site-banner.default-banner.homepage-img -->

<?php elseif ( is_page() ) : ?>

    <section id="site-banner" class="default-banner page-image" role="banner" style="background-image: url('<?php echo magneton_banner( 'small' ) ?>');">
        <div class="container">
            <div class="row">        
                <div class="col-md-12">
                    <h2 class="page-title">
                        <?php echo magneton_get_title(); ?>
                    </h2>
                </div><!-- Ends .col-md-12 -->
            </div><!-- Ends .row -->
            <div class="row">
                <div class="col-md-12">
                    <?php magneton_breadcrumbs(); ?>
                </div><!-- Ends .col-md-6 -->              
            </div><!-- Ends .row -->        
        </div><!-- Ends .container -->
    </section><!-- Ends #site-banner.default-banner.page-image -->
    
<?php else : ?>

    <section id="site-banner" class="default-banner no-image" role="banner">
        <div class="container">
            <div class="row">        
                <div class="col-md-6">
                    <h2 class="page-title">
                    <?php echo magneton_get_title(); ?>
                    </h2>
                </div><!-- Ends .col-md-6 -->
                <div class="col-md-6">
                    <?php magneton_breadcrumbs(); ?>
                </div><!-- Ends .col-md-6 -->              
            </div><!-- Ends .row -->        
        </div><!-- Ends  -->
    </section><!-- Ends #site-banner.default-banner.no-image -->

<?php endif; ?>    
