<?php
/**
 * Displays post meta information
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

 <div class="entry-meta clearfix">
    <ul class="meta-list">
        <li class="meta-date"><i class="fa fa-calendar" aria-hidden="true"></i><?php the_time('F j, Y'); ?></li>
        <li class="meta-author"><i class="fa fa-user" aria-hidden="true"></i><?php the_author(); ?></li>
        <li class="meta-comments"><i class="fa fa-comments" aria-hidden="true"></i><?php comments_number(); ?></li>
        <li class="meta-category"><?php _e( 'Categories: ', 'magneton' ) ?><?php echo get_the_category_list( ', ' ); ?></li>
    </ul><!-- Ends .entry-meta.clearfix -->
</div><!-- Ends .entry-meta -->