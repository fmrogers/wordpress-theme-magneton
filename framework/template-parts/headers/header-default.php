<?php
/**
 * Displays default header
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>
 
<header id="masthead" class="site-header" role="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-5 col-sm-4 col-md-2">                
                <h1 class="site-title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <img src="<?php echo get_template_directory_uri() . '/images/magneton_theme_logo.png' ?>">
                        <span class="sr-only"><?php echo _x( 'Magneton Logo', 'magneton' ); ?></span>
                    </a>
                </h1><!-- Ends .site-title -->
            </div><!-- Ends col-xs-5.col-med-3 -->
            <div class="col-xs-7 col-sm-8">
                <div class="menu-toggle hidden-md hidden-lg pull-right clearfix">        
                    <button class="toggle-button btn btn-default btn-lg pull-right">
                        <i class="fa fa-bars" aria-hidden="true"></i>    
                    </button>
                </div><!-- Ends .mobile-menu-toggle.cleafix -->
            </div><!-- Ends .col-xs-5 -->
            <div class="col-xs-12 col-md-10">
                <?php if ( has_nav_menu( 'main' ) ) : ?>
                    <?php get_template_part( 'framework/template-parts/navigation/navigation', 'main' ); ?>   
                <?php endif; ?>
                <?php if ( has_nav_menu( 'mobile' ) ) : ?>
                    <?php get_template_part( 'framework/template-parts/navigation/navigation', 'mobile' ); ?>   
                <?php endif; ?>
            </div><!-- Ends .col-xs-12.col-md-9 -->
        </div><!-- Ends .row -->
    </div><!-- Ends .container -->
</header><!-- Ends #masthead.site-header -->