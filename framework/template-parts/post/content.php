<?php
/**
 * Displays default post format
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

 <article id="blog-<?php the_ID() ?>" <?php post_class(); ?>>         
            
    <div class="entry-image">
        <?php if ( has_post_thumbnail() ) : ?>
            <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail( 'blog-large' ); ?></a>
        <?php endif; ?>    
    </div><!-- Ends .entry-image -->

    <div class="entry-header clearfix">
        <div class="entry-date clearfix">
            <div class="entry-month"><?php the_time( 'M' ); ?></div>
            <div class="entry-day"><?php the_time( 'j' ); ?></div>    
        </div><!-- Ends .entry-date -->
        <div class="entry-heading">    
            <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
                <h3 class="entry-title"><?php the_title(); ?></h3>
            </a>
        </div><!-- Ends .entry-title -->
    </div><!-- Ends .entry-heading -->

    <?php get_template_part( 'framework/template-parts/meta' ); ?>

    <div class="entry-content">
        <?php if ( !is_single() ) : ?>
            <?php the_excerpt(); ?>
        <?php else : ?>
            <?php the_content(); ?>
        <?php endif; ?>
    </div><!-- Ends .entry-content -->

  
    <?php if ( is_single() ) : ?>
        <!-- Social sharing options -->
        <?php get_template_part( 'framework/template-parts/social-share' ); ?>

        <!-- Post author information -->    
        <div class="author-info clearfix">
            <div class="author-avatar">
                <?php echo get_avatar( get_the_author_meta( 'user_email' ) ); ?>
            </div><!-- Ends .author-avatar -->
            <div class="author-details">            
                <h4 class="author-name">About <?php the_author(); ?></h4>
                <div class="author-description">
                    <?php echo get_the_author_meta('description'); ?>
                </div>
            </div><!-- Ends .author-details -->
        </div><!-- Ends .author-info -->

        <!-- Post comments -->   
        <?php comments_template(); ?>
    <?php endif; ?>   

</article><!-- Ends .post --> 