<?php
/**
 * Displays default archive format
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

 <article id="blog-<?php the_ID() ?>" <?php post_class(); ?>>
    <div class="row">

        <div class="col-sm-4"> 
            <div class="entry-image">
                <?php if ( has_post_thumbnail() ) : ?>
                    <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail( 'blog-medium' ); ?></a>
                <?php endif; ?>    
            </div><!-- Ends .entry-image -->       
        </div><!-- Ends .col-sm-4 -->               

        <div class="col-sm-8">
            <div class="entry-heading">    
                <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
                    <h3 class="entry-title"><?php the_title(); ?></h3>
                </a>
            </div><!-- Ends .entry-title -->
            <div class="entry-content">
                <?php the_excerpt(); ?>
            </div><!-- Ends .entry-content -->
            <?php get_template_part( 'framework/template-parts/meta' ); ?>            
        </div><!-- Ends .col-sm-8 -->        
        
    </div><!-- Ends .row --> 
</article><!-- Ends .post --> 