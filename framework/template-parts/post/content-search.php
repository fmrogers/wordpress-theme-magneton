<?php
/**
 * Displays default search results
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

 <article id="blog-<?php the_ID() ?>" <?php post_class(); ?>>
    <div class="row">

        <div class="col-sm-2"> 
            <div class="result-image">
                <?php if ( has_post_thumbnail() ) : ?>
                    <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail( 'blog-small' ); ?></a>
                <?php endif; ?>    
            </div><!-- Ends .entry-image -->       
        </div><!-- Ends .col-sm-4 -->               

        <div class="col-sm-10">
            <div class="result-heading">    
                <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
                    <h3 class="entry-title"><?php the_title(); ?></h3>
                </a>
            </div><!-- Ends .entry-title -->
            <div class="result-content">
                <?php the_excerpt(); ?>
            </div><!-- Ends .entry-content -->                 
        </div><!-- Ends .col-sm-8 -->        
        
    </div><!-- Ends .row --> 
</article><!-- Ends .post --> 