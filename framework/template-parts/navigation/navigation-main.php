<?php
/**
 * Displays main navigation
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<nav class="main-navigation hidden-xs hidden-sm" role="main navigation" aria-label="<?php _e( 'Main Navigation', 'magneton'); ?>">                  
    <?php wp_nav_menu( array(
        'theme_location' => 'main',
        'menu_id'        => 'main-menu',        
        'menu_class'     => 'menu',
        'container'      => false           
    ) ); ?>
</nav><!-- Ends .main-navigation -->