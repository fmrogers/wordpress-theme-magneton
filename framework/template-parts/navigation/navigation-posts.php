<?php
/**
 * Displays previous/next post navigation
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>
 
<div class="post-navigation page-left">    
    <?php previous_post_link('%link', '<i class="fa fa-chevron-left" aria-hidden="true"></i>'); ?>
</div>

<div class="post-pagination page-right">    
    <?php next_post_link('%link', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'); ?>
</div>           
