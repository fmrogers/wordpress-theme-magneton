<?php
/**
 * Displays post pagination
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<div class="post-pagination">
    <?php echo paginate_links(); ?>
</div>
