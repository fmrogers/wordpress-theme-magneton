<?php
/**
 * Displays mobile navigation
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<nav class="mobile-navigation hidden-md hidden-lg clearfix" role="navigation" aria-label="<?php _e( 'Mobile Navigation', 'magneton'); ?>">                  
    <?php wp_nav_menu( array(
        'theme_location' => 'mobile',
        'menu_id'        => 'mobile-menu',        
        'menu_class'     => 'menu',
        'container'      => false           
    ) ); ?>
</nav><!-- Ends .mobile-navigation -->