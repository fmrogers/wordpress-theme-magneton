<?php
/**
 * Displays topbar navigation
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>
<section id="topbar" class="topbar clearfix" role="topbar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">            
                        <div class="topbar-contact togglable" role="contact-information" aria-label="<?php _e( 'Top Bar Contact Information', 'magneton'); ?>">
                            <ul class="menu">
                                <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+4769516524">+47 695 16 524</a></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:magneton@carbonium.no">magneton@carbonium.no</a></li>                    
                            </ul>
                        </div><!-- Ends .top-navigation -->           
                    </div><!-- Ends .col-md-6 -->
                    <div class="col-md-6">           
                        <nav class="topbar-navigation togglable hidden-xs hidden-sm" role="navigation" aria-label="<?php _e( 'Top Bar Menu', 'magneton'); ?>">                
                            <?php if ( has_nav_menu( 'topbar' ) ) : ?>
                                <?php wp_nav_menu( array(
                                    'theme_location' => 'topbar',        
                                    'menu_class'     => 'menu',
                                    'container'      => false           
                                ) ); ?> 
                            <?php endif; ?>                                                       
                        </nav><!-- Ends .top-navigation -->            
                    </div><!-- Ends .col-md-6 -->             
                    <div class="col-md-12">            
                        <?php $mainNav_id = esc_attr( uniqid( 'search-form-' ) ); ?>
                        <form id="main-search" class="main-search togglable" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <div class="search-form">            
                                <label class="sr-only" for="<?php echo $mainNav_id; ?>">
                                    <span class="sr-only"><?php echo _x( 'Search for:', 'label', 'magneton' ); ?></span>
                                </label>
                                <input id="<?php echo $mainNav_id; ?>" class="search-field" type="search"  placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'magneton' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />            
                            </div><!-- Ends .search-form -->
                        </form><!-- Ends #main-search -->                                
                    </div><!-- Ends .col-md-12 -->
                    <button class="toggle-search-btn" role="toggle-search">
                        <i class="togglable fa fa-search" aria-hidden="true"></i>
                        <i class="togglable fa fa-times" aria-hidden="true" style="display: none;"></i>
                        <span class="sr-only"><?php echo _x( 'Toggle Search', 'magneton' ); ?></span>    
                    </button>  
                </div><!-- Ends .row -->
            </div><!-- Ends .col-md-12 -->
        </div><!-- Ends .row -->
    </div><!-- Ends .container -->    
</section><!-- Ends #topbar -->