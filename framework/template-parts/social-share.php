<?php
/**
 * Displays default post format
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<div class="post-share">
    <ul>
        <li><a href="javascript:void(0)"><i class="icon fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
        <li><a href="javascript:void(0)"><i class="icon fa fa-twitter" aria-hidden="true"></i>Twitter</a></li>
        <li><a href="javascript:void(0)"><i class="icon fa fa-linkedin" aria-hidden="true"></i>LinkedIn</a></li>
        <li><a href="javascript:void(0)"><i class="icon fa fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        <li><a href="javascript:void(0)"><i class="icon fa fa-google-plus" aria-hidden="true"></i>Google+</a></li>
        <li><a href="javascript:void(0)"><i class="icon fa fa-youtube-play" aria-hidden="true"></i>YouTube</a></li>
        <li><a href="javascript:void(0)"><i class="icon fa fa-envelope" aria-hidden="true"></i><?php _e( 'E-Mail', 'magneton' ); ?></a></li>
    </ul>
</div><!-- Ends .post-share -->