 <?php
 /** 
  * Set title of page conditionally
  * 
  * @author Frederick M. Rogers of Carbonium Designs
  * @package WordPress
  * @subpackage Magneton 
  *  
  */

function magneton_get_title() {
    if ( is_home() ) {
        $title = 'Blog';
    } else {
        $title = get_the_title();
    }
    return $title;
}
 
 
 