 <?php
 /** 
  * Set featured image background as url
  * 
  * @author Frederick M. Rogers of Carbonium Designs
  * @package WordPress
  * @subpackage Magneton 
  *  
  */

function magneton_banner( $size ) {

    if ( !has_post_thumbnail() || ( is_front_page() && is_home() ) )  {
        if ( $size == 'large' ) {
            $url = get_template_directory_uri() . '/images/default_featured_image-[homepage].jpg';
        } elseif ( $size == 'small' ) {
            $url = get_template_directory_uri() . '/images/default_featured_image-[page].jpg';
        }
    } else {
        $url = the_post_thumbnail_url('full');
    }    

    return $url; 

}


