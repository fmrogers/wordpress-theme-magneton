<?php
/** 
 * Displays theme header
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <title>
            <?php wp_title( '|', true, 'right' ); ?>
            <?php bloginfo( 'name' ); ?>
        </title>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>
    </head>
    
    <body <?php body_class(); ?>>
                   
        <?php get_template_part( 'framework/template-parts/navigation/navigation', 'top' ); ?>                   
        <?php get_template_part( 'framework/template-parts/headers/header', 'default' ); ?>
        <?php get_template_part( 'framework/template-parts/banners/banner', 'default' ); ?>