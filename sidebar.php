<?php
/**
 * Displays main sidebar area
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<div id="sidebar" class="sidebar">

    <?php if ( is_page() ) : ?>

        <?php dynamic_sidebar( 'page-sidebar' ); ?>

    <?php else : ?>

        <?php dynamic_sidebar( 'blog-post-sidebar' ); ?>

    <?php endif; ?>

</div><!-- Ends #sidebar.site-sidebar -->