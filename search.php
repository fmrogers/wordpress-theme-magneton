<?php
/**
 * Displays search results page
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<?php get_header(); ?>

<section id="content" class="site-content search-page">
    <div class="container">
        <div class="row">

            <div class="col-md-9">

                <div class="new-search">                    
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="title"><?php _e('New Search', 'magneton') ?></h3>
                            <p class="text"><?php _e('If you did not find what you where looking for please try again', 'magneton') ?></p>                            
                        </div>
                        <div class="col-md-6">
                            <?php get_search_form(); ?>    
                        </div>                  
                    </div>                    
                </div>

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php get_template_part( 'framework/template-parts/post/content', 'search' ); ?>                
                <?php endwhile; else : ?>        
                    <p><?php _e( 'Sorry, no postes matched your criteria' ); ?></p>         
                <?php endif; ?>
                <?php get_template_part( 'framework/template-parts/navigation/navigation', 'pagination' ); ?>
            </div><!-- Ends .col-md-9 -->

            <div class="col-md-3">
                <?php get_sidebar() ?>            
            </div><!--- Ends .col-md-3 -->

        </div><!-- Ends .row -->
    </div><!-- Ends .container -->          

<?php get_footer(); ?>