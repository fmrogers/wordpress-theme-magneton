        </section><!-- Ends #content.site-content -->

        <footer id="footer" class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="footer-sidebars">
                        <div class="col-md-3">
                            <div id="footer-sidebar-1" class="sidebar">
                                <?php dynamic_sidebar( 'footer-sidebar-one' ); ?>
                            </div><!-- #footer-sidebar-1.sidebar -->
                        </div><!-- Ends .col-md-3 -->
                        <div class="col-md-3">
                            <div id="footer-sidebar-2" class="sidebar">
                                <?php dynamic_sidebar( 'footer-sidebar-two' ); ?>
                            </div><!-- #footer-sidebar-2.sidebar -->
                        </div><!-- Ends .col-md-3 -->
                        <div class="col-md-3">
                            <div id="footer-sidebar-3" class="sidebar">
                                <?php dynamic_sidebar( 'footer-sidebar-three' ); ?>
                            </div><!-- #footer-sidebar-2.sidebar -->
                        </div><!-- Ends .col-md-3 -->
                        <div class="col-md-3">
                            <div id="footer-sidebar-4" class="sidebar">
                                <?php dynamic_sidebar( 'footer-sidebar-four' ); ?>
                            </div><!-- #footer-sidebar-2.sidebar -->
                        </div><!-- Ends .col-md-3 -->
                    </div><!-- Ends .footer-sidebars -->
                </div><!-- Ends .row -->
            </div><!-- Ends .container -->
        </footer>                

        <div id="bottombar" class="bottombar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="copyright-area">
                            <span>&copy; 2017 Magneton - WordPress Theme built by Carbonium Designs Using WordPress</span>
                        </div><!-- Ends copyright-area -->
                    </div><!-- Ends .col-md-6 -->
                    <div class="col-md-6">
                        <div class="social-media">
                            <ul>
                                <li><a href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>                                    
                            </ul>
                        </div><!-- Ends .social-media -->
                    </div><!-- Ends .col-md-6 -->
                </div><!-- Ends .row --> 
            </div><!-- Ends .container -->
        </div><!-- #bottombar.bottombar -->       
        
        <?php wp_footer(); ?>

        <script id="magneton">

            (function($) {  

                $('body').magneton();

            })(jQuery);

        </script>
    </body>
</html>