;(function ( $, window, document, undefined ) {
    
    var pluginName = "magneton",
        defaults = {
            breakpoint: {
                small: 768,
                medium: 992,
                large: 1200 
            } 
        };

   function Plugin( element, options ) {

        this.element = $(element);

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init(this.element, this.options);
    }

    Plugin.prototype = {

        init: function($el, opt) {

            /* Toggles search field in topbar */
            $el.find('.toggle-search-btn').on('click', function(event) {
                $el.find('#topbar .togglable').toggle(0, function() {
                    if ( $('#topbar .search-field').is(':visible') ) {
                        $('.topbar-navigation').hide();
                    } else {
                        $('.topbar-navigation').show();
                    }
                });
            });

            /* Initialize external libraries */                           
            $('ul#main-menu').superfish();  // Superfish
            $('ul#mobile-menu').magnav({
                toggleSelector: '.menu-toggle .toggle-button'
            });             
            
        }        
        
    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );

