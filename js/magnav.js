;(function ( $, window, document, undefined ) {
    'use strict'
    var pluginName = "magnav",
        prefix = pluginName,
        defaults = {
            toggleSelector: '.toggle-menu',
            arrows: {
                open: 'fa-angle-down',
                closed: 'fa-angle-right'
            },
            speed: 200
        };

    function Plugin( element, options ) {

        this.element = $(element);

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init(this.element, this.options);
    }

    Plugin.prototype = {

        init: function($el, opt) {            
            // If 'ul' is not visible 
            if ( $el.find('ul').is(':hidden') ) {
                // Add a class of '_hidden'
                $el.find('ul').addClass(prefix + '-hidden');
                // Add a class of '_collapsed'
                $el.find('ul').parent().addClass(prefix + '-collapsed');
            }
            
            // If 'ul' has a parent element of 'li'
            if ( !$el.find('ul').parents('li').hasClass(prefix + '-parent') ) {
                // Add class of '_parent'
                $el.find('ul').parents('li').addClass(prefix + '-parent');            
            }
             
            // If 'ul' parent element has a class of '_parent'
            if ( $el.find('ul').parents('li').hasClass(prefix + '-parent') ) {
                // Append a element with a class of '_arrow'
                $('.' + prefix + '-parent').find(' > a').append('<span class="' + prefix + '-arrow"><i class="fa ' + opt.arrows.closed + '" aria-hidden="true"></i></span>');                
            }

            // Bind event handler to menu toggle            
            $(opt.toggleSelector).on('click', function(event) {
                var $this = $(this),
                    $menu = $el;                    
                if ( $menu.parent().is(':hidden') ) {                    
                    $menu.parent().slideDown(opt.speed);
                } else {
                    $menu.parent().slideUp(opt.speed);
                }
            });            
            
            // Bind event handler to menu items
            $el.find('.' + prefix + '-arrow').on('click', function(event) {                               
                var $this = $(this),
                    $parent = $this.parent().parent();
                // Prevent secondary events from fiering
                if ( $(event.target).not($this) ) {
                    event.preventDefault();
                }                
                // If this has a class of '_arrow'                
                if ( $parent.hasClass(prefix + '-collapsed') ) {
                    $parent.removeClass(prefix + '-collapsed').find('ul').first().slideDown();
                    $this.find('.fa').removeClass(opt.arrows.closed).addClass(opt.arrows.open);
                } else {                
                    $parent.addClass(prefix + '-collapsed').find('ul').first().slideUp();
                    $this.find('.fa').removeClass(opt.arrows.open).addClass(opt.arrows.closed);
                }

                 event.stopPropagation();

            });
        }
    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );