<?php
/**
 * Displays a 404 error page
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<?php get_header(); ?>

<section id="content" class="site-content search-page">
    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <div class="heading404">
                    <h2 class="title404">404 <small class="subtitle404"><?php _e( 'Not Found', 'magneton' ) ?></small></h2>                     
                </div>

                <div class="content404">
                    <p><?php _e( 'The page you are looking for does not appear to be at this location', 'magneton' ) ?></p>
                </div>

                <div class="search404">                    
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <h3 class="title"><?php _e('Try searching!', 'magneton') ?></h3>
                            <p class="text"><?php _e('If you did not find what you where looking for try searching for it', 'magneton') ?></p>
                            <?php get_search_form(); ?>                            
                        </div>                                        
                    </div>                    
                </div>
                
            </div><!-- Ends .col-md-12 -->            

        </div><!-- Ends .row -->
    </div><!-- Ends .container -->          

<?php get_footer(); ?>