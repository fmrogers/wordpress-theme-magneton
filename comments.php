<?php
/**
 * Displays default comment structure
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

if ( post_password_required() ) {
    return;
}

?>

<div class="comments-area">

    <?php if ( have_comments() ) : ?>

        <h4 class="comments-title">
            <?php comments_number(); ?>
        </h4>

        <ol class="comments-list">
            <?php wp_list_comments( array( 'callback' => 'magneton_comment_list' ) ); ?>
        </ol>        
        
    <?php endif; ?>

    <?php if ( !comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments'  ) ) : ?>

            <p class="no-comments"><?php _e( 'Comments are closed', 'magneton' ); ?></p>

        <?php endif; ?>

    <?php if ( comments_open() ) : ?>

        <?php $comments_args = array(                
            'id_form'       => 'comment-form',
            'class_form'    => 'comment-form',
            'class_submit'  => 'btn btn-default',
            'title_reply' => __( 'Leave a reply', 'magneton' ),                
            'label_submit' => __( 'Submit Comment', 'magneton' )
        ); ?>

        <?php if ( !is_user_logged_in() ) : ?>

            <?php comment_form( $comments_args ); ?>         

        <?php else : ?>

            <?php $comments_args[ 'comment_field' ] = '<textarea name="comment" id="comment" class="form-control comment-field"  aria-required="true" rows="10"></textarea>'; ?>

            <?php comment_form( $comments_args ); ?> 

        <?php endif ?>

    <?php endif; ?>

      

</div><!-- Ends .post-comments -->
 

