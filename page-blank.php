<?php
/**
 * Template Name: Full Width Page
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<?php get_header(); ?>

<section id="content" class="site-content page">

    <div class="container">

        <div class="row">

            <div class="col-md-12">                   

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>                         

                    <?php the_content(); ?>            
                
                <?php endwhile; endif; ?>

            </div><!-- Ends .col-md-9 -->            

        </div><!-- Ends .row -->       

    </div><!-- Ends .container -->      

<?php get_footer(); ?>