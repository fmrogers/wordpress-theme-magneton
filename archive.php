<?php
/**
 * Displays archive page
 *
 * @package WordPress
 * @subpackage Magneton
 * @since 1.0
 * @version 1.0
 */

 ?>

<?php get_header(); ?>

<section id="content" class="site-content archive-page">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php get_template_part( 'framework/template-parts/post/content', 'archive' ); ?>                
                <?php endwhile; else : ?>        
                    <p><?php _e( 'Sorry, no postes matched your criteria' ); ?></p>         
                <?php endif; ?>
                <?php get_template_part( 'framework/template-parts/navigation/navigation', 'pagination' ); ?>
            </div><!-- Ends .col-md-9 -->

            <div class="col-md-3">
                <?php get_sidebar() ?>            
            </div><!--- Ends .col-md-3 -->

        </div><!-- Ends .row -->
    </div><!-- Ends .container -->          

<?php get_footer(); ?>