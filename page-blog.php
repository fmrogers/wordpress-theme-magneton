<?php
/*
 ** Template Name: Full Width Blog
 */
?>

<?php get_header(); ?>

<section id="content" class="site-content full-width blog-page">

    <div class="container">       

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>                         

            <?php get_template_part( 'framework/template-parts/post/content', get_post_format() ); ?>            
        
        <?php endwhile; ?>            

            <?php get_template_part( 'framework/template-parts/navigation/navigation', 'pagination' ); ?>            
        
        <?php else : ?>

            <p><?php _e( 'Sorry, no postes matched your criteria' ); ?></p>

        <?php endif; ?>        

    </div><!-- Ends .container -->       

<?php get_footer(); ?>